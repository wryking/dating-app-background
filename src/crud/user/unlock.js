export const tableOption = {
  border: true,
  selection: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  menuWidth: 350,
  align: 'center',
  refreshBtn: true,
  searchSize: 'mini',
  addBtn: false,
  editBtn: false,
  delBtn: false,
  viewBtn: false,
  props: {
    label: 'label',
    value: 'value'
  },
  column: [{
    label: '解锁人',
    prop: 'name'
  },{
    label: '被解锁人',
    prop: 'unlockedUserName'
  },{
    label: '解锁时间',
    prop: 'unlockTime'
  },{
    label: '解锁过期时间',
    prop: 'passedTime'
  }, {
    label: '解锁价格',
    prop: 'unlockPrice'
  },{
    label: '解锁类型',
    prop: 'unlockType',
    type: 'select',
    slot: true,
    dicData: [
      {
        label: 'vip',
        value: 1
      }, {
        label: '钻石',
        value: 2
      }
    ]
  }, {
      label: '解锁状态',
      prop: 'status',
      dicData: [
        {
          label: '已过期',
          value: 1
        }, {
          label: '未过期',
          value: 2
        }
      ]
    }
]
}
