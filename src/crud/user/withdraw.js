export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  align: 'center',
  addBtn: false,
  editBtn: false,
  delBtn: false,
  viewBtn: false,
  column: [
    {
      label: '用户名',
      prop: 'xingMing'
    },
    {
      label: '银行名称',
      prop: 'yinHang'
    },
    {
      label: '银行卡号',
      prop: 'kaHao'
    },
    {
      label: '提现时间',
      prop: 'carryTime'
    },
    {
      label: '提现金额',
      prop: 'rmb'
    },
    {
      label: '提现状态',
      prop: 'state',
      dicData: [
        {
          label: '已通过',
          value: 1
        }, {
          label: '未通过',
          value: 2
        }, {
          label: '待处理',
          value: 3
        }
      ]
    },
    {
      label: '提现用户id',
      prop: 'userId'
    },
    {
      label: '处理备注',
      prop: 'remarks'
    }
  ]
}
