export const userlabel = {
  border: true,
  selection: true,
  index: false,
  indexLabel: '序号',
  stripe: true,
  menuAlign: 'center',
  menuWidth: 350,
  align: 'center',
  refreshBtn: true,
  searchSize: 'mini',
  addBtn: false,
  editBtn: false,
  delBtn: false,
  viewBtn: false,
  props: {
    label: 'label',
    value: 'value'
  },
  column: [{
    label: '充值数量',
    prop: 'prodName'
  }, {
    label: '操作人',
    prop: 'userName'
  },{
    label: '金额统计',
    prop: 'actualTotal'
  },
  // {
  //   label: '地区',
  //   prop: 'actualTotal'
  // },
  {
    label: '时间',
    prop: 'createTime',
    search:true,
    type:"daterange"
  },{
    label: '支付方式',
    prop: 'payType',
    dicData: [
      {
        label: '手动代付',
        value: 0
      }, {
        label: '微信支付',
        value: 1
      }, {
        label: '支付宝',
        value: 2
      }
    ]
  },{
    label: '状态',
    prop: 'isPayed',
    search:true,
    type:"select",
    dicData: [
      {
        label: '没有支付',
        value: 0
      }, {
        label: '付款成功',
        value: 1
      }
    ]
  }
]
}
